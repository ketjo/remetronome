import { nanoid } from 'nanoid';

export const initSong = {
  id: '0',
  beats: 4,
  size: 4,
  bpm: 120,
  author: '',
  name: '',
  accent: true,
};

export const useSystemStore = defineStore('system', {
  state: () => ({
    songList: [],
    settings: {
      soundType: 'click',
      volume: -5,
      theme: 'dark',
    },
    currentSong: initSong,
    displayName: '',
    email: '',
    avatarUrl: '',
    userId: '',
    authorized: false,
    appLoading: true,
    isPlaying: false,
    songListOpen: false,
    userMenuOpen: false,
  }),
  getters: {
    valuesToSave: (state) => {
      return {
        currentSongId: state.currentSong.id,
        songList: state.songList,
        settings: state.settings,
      };
    },
    isCurrentSongDefault: (state) => state.currentSong.id === '0'
  },
  actions: {
    authorize(userData) {
      this.userId = userData.uid;
      this.displayName = userData.displayName;
      this.email = userData.email;
      this.avatarUrl = userData.photoURL;
      this.authorized = true;
    },
    unauthorize() {
      this.userId = '';
      this.displayName = '';
      this.email = '';
      this.avatarUrl = '';
      this.authorized = false;
    },
    loadSongState(data) {
      this.settings = data?.settings ?? this.settings;
      this.songList = data?.songList ?? [];
      const songCandidate = this.songList.find(
        (song) => song.id === data?.currentSongId
      );
      this.currentSong = songCandidate ? songCandidate : initSong
    },
    changeTheme(theme) {
      this.settings.theme = theme
    },
    saveSongChanges() {
      const songIndex = this.songList.findIndex(
        (song) => song.id === this.currentSong.id
      );
      if (!this.songList[songIndex]) return;
      this.songList[songIndex] = { ...this.currentSong };
    },
    setSong(id) {
      const song = this.songList.find((song) => song.id === id);
      this.currentSong = song;
    },
    saveSong(song) {
      this.songList.unshift({ ...song, id: nanoid() });
    },
    updateSong(songToUpdate) {
      const songIndex = this.songList.findIndex(
        (song) => song.id === songToUpdate.id
      );
      this.songList[songIndex] = songToUpdate;
    },
    saveSpotifySong(songData) {
      this.songList.unshift({ ...initSong, ...songData });
    },
    deleteSong(id) {
      if (id === this.currentSong.id) this.currentSong = initSong;
      const index = this.songList.findIndex((song) => song.id === id);
      this.songList.splice(index, 1);
    },
    setFirstSong () {
      this.currentSong = this.songList[0]
    },
    setDefaultSong () {
      this.currentSong = initSong;
    }
  },
});
