export default defineNuxtPlugin((nuxtApp) => {
  const config = useRuntimeConfig();
  const { spotifyClientId, spotifyClientSecret } = config.public.spotify;
  const actions = {
    getToken: () => {
      return {
        endpoint: 'https://accounts.spotify.com/api/token',
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: `Basic ${btoa(
            `${spotifyClientId}:${spotifyClientSecret}`
          )}`,
        },
        body: 'grant_type=client_credentials',
      };
    },
    findTracks: ({ query, token }) => {
      return {
        endpoint: `https://api.spotify.com/v1/search?q=${query}&type=track`,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: null,
      };
    },
    getBPMBySongId: ({ id, token }) => {
      return {
        endpoint: `https://api.spotify.com/v1/audio-features/${id}`,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
        body: null,
      };
    },
  };

  const spotifyService = async (action, payload) => {
    const { endpoint, method, headers, body } = actions[action](payload);
    const metaData = { method, headers };
    if (body) metaData.body = body;
    return fetch(endpoint, metaData);
  };

  nuxtApp.provide('spotify', spotifyService);
});
