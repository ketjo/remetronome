import { initializeApp } from 'firebase/app';
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { ref, set, get } from 'firebase/database';

export default defineNuxtPlugin((nuxtApp) => {
  const config = useRuntimeConfig();
  const app = initializeApp(config.public.firebase);
  const auth = getAuth(app);
  const firestore = getDatabase(app);

  const updateFbValue = async (userId, payload) => {
    try {
      return await set(ref(firestore, `users/${userId}`), payload)
    } catch (error) {
      console.error('Error updating data in Firebase:', error);
    }
  };

  const getFbData = (userId) => get(ref(firestore, `users/${userId}`));
  nuxtApp.provide('firebase', { auth, updateFbValue, getFbData })
});
