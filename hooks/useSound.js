import { ref } from 'vue';
import { useSystemStore } from '@/store';
import * as Tone from 'tone';

export const useSound = () => {
  let loop = null;
  const beatCounter = ref(0);
  const sounds = {
    click: {},
    'rim-shot': {},
    'hi-hat': {},
  };
  let sndHigh = null;
  let sndLow = null;
  const beatsDOM = ref([]);

  const store = useSystemStore();
  let toneVolumeNode = store.settings.volume;

  onMounted(() => {
    loadSound();
    setSound();
    Tone.Transport.bpm.value = store.currentSong.bpm;
  });

  const allSounds = ['click', 'hi-hat', 'rim-shot'];
  const loadSound = () => {
    toneVolumeNode = new Tone.Volume(store.settings.volume).toDestination();
    const baseUrl = 'sounds/';
    allSounds.forEach((sound) => {
      sounds[sound].high = new Tone.Player(`${baseUrl}${sound}/1.mp3`).connect(
        toneVolumeNode
      );
      sounds[sound].low = new Tone.Player(`${baseUrl}${sound}/0.mp3`).connect(
        toneVolumeNode
      );
    });
  };

  const setSound = () => {
    sndHigh = sounds[store.settings.soundType].high;
    sndLow = sounds[store.settings.soundType].low;
  };

  const createLoop = () => {
    loop = new Tone.Loop((time) => {
      const isFirstBeatAccent =
        beatCounter.value === 0 && store.currentSong.accent;
      isFirstBeatAccent ? sndHigh.start(time) : sndLow.start(time);
      Tone.Draw.schedule(() => {
        makeGlowBeatDOM(beatCounter.value, isFirstBeatAccent);
        beatCounter.value = beatCounter.value + 1;
      }, time);
    }, `${store.currentSong.size}n`).start(0);
  };

  watch(
    () => [store.isPlaying],
    ([newVal]) => {
      if (newVal) play();
      else stop();
    }
  );

  const updateBPM = () => {
    const BPM = store.currentSong?.bpm;
    if (!BPM) return;
    Tone.Transport.bpm.value = BPM;
  };

  watch(beatCounter, (newVal) => {
    if (newVal >= store.currentSong.beats) beatCounter.value = 0;
  });

  let prevDOMEl = null;
  let prevBeatModifier = false;

  const clearLastGlowBeat = () => {
    if (!prevDOMEl) return;
    prevDOMEl.classList.remove(`beats-indicator__element--${prevBeatModifier}`);
  };

  const clearAllGlowBeat = () => {
    const timerId = setTimeout(() => {
      beatsDOM.value.forEach((beatDOM) => {
        beatDOM.classList.remove('beats-indicator__element--accent-blue');
        beatDOM.classList.remove('beats-indicator__element--accent-yellow');
      });
      clearTimeout(timerId);
    }, 100);
  };

  const makeGlowBeatDOM = (counter, isFirstBeatAccent) => {
    clearLastGlowBeat();
    const currentDOMEl = beatsDOM.value[counter];
    const modifier = isFirstBeatAccent ? 'accent-yellow' : 'accent-blue';
    currentDOMEl.classList.add(`beats-indicator__element--${modifier}`);
    prevDOMEl = currentDOMEl;
    prevBeatModifier = modifier;
  };

  const soundIdx = ref(0);
  const updateSound = () => {
    soundIdx.value++;
    if (soundIdx.value >= allSounds.length) soundIdx.value = 0;
    store.settings.soundType = allSounds[soundIdx.value];
    setSound();
  };

  const play = async () => {
    createLoop();
    await Tone.start();
    Tone.Transport.start();
  };

  const stop = async () => {
    Tone.Transport.stop();
    beatCounter.value = 0;
    loop.dispose();
    clearAllGlowBeat();
  };

  const updateBeatsDOM = (beats) => {
    beatsDOM.value = beats;
  };

  const updateSize = () => {
    const size = store.currentSong?.size;
    if (!size || !loop) return;
    loop.interval = `${size}n`;
  };

  const updateVolume = (newValue) => {
    toneVolumeNode.volume.value = newValue;
  };

  return {
    allSounds,
    updateSound,
    updateBeatsDOM,
    updateBPM,
    updateSize,
    updateVolume,
  };
};
