import { ref, onMounted } from 'vue';
import { useSystemStore } from '../store';

export const useTheme = () => {
  const store = useSystemStore();
  const htmlElDOM = ref(null);
  const key = 'data-theme';

  const setRootElement = () => {
    if (htmlElDOM.value) return
    htmlElDOM.value = document.querySelector('html');
  }

  const loadTheme = () => {
    const theme = localStorage.getItem(key);
    if (theme) changeTheme(theme);
  }

  const changeTheme = (theme) => {
    if (!htmlElDOM.value) return
    htmlElDOM.value.setAttribute(key, theme);
    localStorage.setItem(key, theme);
    store.changeTheme(theme)
  }

  onMounted(setRootElement);

  return {
    changeTheme,
    loadTheme,
    setRootElement
  };
}
