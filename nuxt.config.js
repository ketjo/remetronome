export default defineNuxtConfig({
  app: {
    head: {
      title: 'Remetronome',
      link: [
        {
          rel: 'icon',
          type: 'image/png',
          href: './additional-data/favicon-32x32.png',
        },
      ],
    },
  },
  css: ['~/styles/style.scss'],
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `@use "@/styles/mixins.scss" as *;`,
        },
      },
    },
  },
  components: ['~/components/'],
  modules: ['@pinia/nuxt', '@vite-pwa/nuxt', 'nuxt-snackbar'],
  snackbar: {
    top: true,
    left: true,
    duration: 4000,
    success: '#5de65ade',
    error: '#c45656de',
    info: '#109ef1',
  },
  pinia: {
    autoImports: ['defineStore', ['defineStore', 'definePiniaStore']],
  },
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
    ],
  },
  runtimeConfig: {
    public: {
      spotify: {
        spotifyClientId: process.env.SPOTIFY_CLIENT_ID,
        spotifyClientSecret: process.env.SPOTIFY_CLIENT_SECRET,
      },
      firebase: {
        apiKey: process.env.FIREBASE_API_KEY,
        authDomain: 'metronome-6e1bd.firebaseapp.com',
        databaseURL: 'https://metronome-6e1bd-default-rtdb.firebaseio.com',
        projectId: 'metronome-6e1bd',
        storageBucket: 'metronome-6e1bd.appspot.com',
        messagingSenderId: '976550974028',
        appId: '1:976550974028:web:d0e1b106a563b7ee6e9ae9',
        measurementId: 'G-4ML26JDBG7',
      },
    },
  },
  pwa: {
    meta: {
      mobileApp: true,
      appleStatusBarStyle: 'black-translucent',
    },
    manifest: {
      name: 'Remetronome',
      short_name: 'Remetronome',
      description:
        'The app for musicians who need to always have the right tempo. The application also provides the ability to find and save songs bpm and size.',
      theme_color: '#212529',
      msTileColor: '#212529',
      background_color: '#212529',
      display: 'standalone',
      orientation: 'portrait',
      icons: [
        {
          src: './additional-data/android-chrome-192x192.png',
          sizes: '192x192',
          type: 'image/png',
        },
        {
          src: './additional-data/android-chrome-512x512.png',
          sizes: '512x512',
          type: 'image/png',
        },
        {
          src: './additional-data/android-chrome-maskable-192x192.png',
          sizes: '192x192',
          type: 'image/png',
          purpose: 'maskable',
        },
        {
          src: './additional-data/android-chrome-maskable-512x512.png',
          sizes: '512x512',
          type: 'image/png',
          purpose: 'maskable',
        },
      ],
    },
    registerType: 'autoUpdate',
    workbox: {
      navigateFallback: '/',
      cleanupOutdatedCaches: true,
      skipWaiting: true,
      clientsClaim: true,
    },
    client: {
      installPrompt: true,
      periodicSyncForUpdates: 1000  ,
    },
    devOptions: {
      navigateFallbackAllowlist: [/^\/$/],
      enabled: true,
      type: 'module',
      suppressWarnings: true,
    },
  },
});
