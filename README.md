# Remetronome

An app for musicians who need to always have the right tempo. The application also provides the ability to find save and edit songs.


[You can try it here!](https://remetronome-ketjo-61df86c96dd264e127b63809f1fc8e44d6391730c5fac.gitlab.io/)


![QR-link](./public/additional-data/qr.png 'qr link')

### Contributions

[Ketjo](https://gitlab.com/users/ketjo/projects)

## App main screen

![App main screen](./public/additional-data/main-page-screen.png 'main screen')

## Contributing

Changes and improvements are more than welcome! Feel free to fork and open a pull request. Please make your changes in a specific branch and request to pull into `dev`! If you can, please make sure the game fully works before sending the MR, as that will help speed up the process.

## Recommendation
NPM as package manager
Node 18.13.0


### Attention
Do not forget add global variables
Here is auto deployment when a branch merge to "main"
To start develop run 'npm run dev'