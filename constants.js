export const constants = {
  bpmMin: 20,
  bpmMax: 300,
  beatsMin: 1,
  beatsMax: 16,
  sizeMin: 2,
  sizeMax: 16,
  minVolume: -35,
  scrollPower: 50,
  volumeStep: 0.01

};
