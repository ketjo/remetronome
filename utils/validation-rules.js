export const emailRule = {
  regExp: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,}$/i,
  text: 'Not valid email',
}

export const fieldRequired = {
  regExp: /^.+$/,
  text: 'Field is required',
}

export const passwordRule = {
  regExp: /^(?=.*[0-9])(?=.*[!@#$%^&*])[A-Za-z0-9!@#$%^&*]{6,}$/,
  text: 'The password must contain at least 6 characters, one digit and one special character',
}