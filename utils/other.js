export const capitalizeFirstChar = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
};

export const bpmBlurHandler = (event, constants) => {
  const value = Number(event.srcElement.value);
  const { bpmMin, bpmMax } = constants;

  if (value < bpmMin) {
    event.srcElement.value = bpmMin;
  }
  if (value > bpmMax) {
    event.srcElement.value = bpmMax;
  }
};